package topic;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import utils.RabbitMQUtils;

import java.io.IOException;

/**
 * 第五种模型（Routing-Topic），动态路由模式
 */
public class Provider {

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        Channel channel = connection.createChannel();

        String exchangeName = "topics";

        // 声明交换机以及交换机类型
        channel.exchangeDeclare(exchangeName, "topic");

        // 发布消息
        String routeKey = "save.user.delete";
        channel.basicPublish(exchangeName, routeKey, null, ("这里是topic动态路由模型，route key：["+routeKey+"]").getBytes());

        // 关闭资源
        RabbitMQUtils.closeConnectionAndChannel(channel, connection);
    }
}
