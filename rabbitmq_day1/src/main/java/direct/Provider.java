package direct;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import utils.RabbitMQUtils;

import java.io.IOException;

/**
 * 第四种模型（Routing-Direct），路由模式
 */
public class Provider {

    public static void main(String[] args) throws IOException {
        // 获取连接对象
        Connection connection = RabbitMQUtils.getConnection();
        // 获取连接通道对象
        Channel channel = connection.createChannel();

        String exchangeName = "logs_direct";
        // 通过通道声明我们交换机
        // 参数1：交换机名称
        // 参数2：交换机类型，direct-路由模式
        channel.exchangeDeclare(exchangeName, "direct");

        // 发送消息
        String routingKey = "error";
//        String routingKey = "info";
        channel.basicPublish(exchangeName, routingKey, null, ("这是direct模型发布的基于route key：["+routingKey+"]发送的消息").getBytes());

        // 关闭资源
        RabbitMQUtils.closeConnectionAndChannel(channel, connection);
    }
}
