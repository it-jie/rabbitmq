package com.baizhi.test;

import com.baizhi.RabbitmqSpringbootApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = RabbitmqSpringbootApplication.class)
@RunWith(SpringRunner.class)
public class TestRabbitMQ {

    // 注入rabbitTemplate
    @Autowired
    private RabbitTemplate rabbitTemplate;

    // hello world模型
    @Test
    public void testHelo() {
        rabbitTemplate.convertAndSend("hello", "hello world");
    }

    // work模型
    @Test
    public void testWork() {
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("work", "work模型"+i);
        }
    }

    // fanout广播模型
    @Test
    public void testFanout() {
        rabbitTemplate.convertAndSend("logs", "", "Fanout模型发送的消息");
    }

    // route路由模型
    @Test
    public void testRoute() {
//        rabbitTemplate.convertAndSend("directs", "info", "发送info的key的路由信息");
        rabbitTemplate.convertAndSend("directs", "error", "发送error的key的路由信息");
    }

    // topic动态路由(订阅模式)
    @Test
    public void testTopic() {
//        rabbitTemplate.convertAndSend("topics", "user.save", "user.save 路由消息");
        rabbitTemplate.convertAndSend("topics", "product", "product 路由消息");
    }
}
